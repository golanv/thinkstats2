(ns data
  (:require [clj-toolbox.db :as db]
            [tablecloth.api :as tc]))

(def FemPregTable (db/results-map "thinkstats2" "public" "\"2002FemPreg\""))
(def FemPregDS (->> "FemPreg"
                    (tc/set-dataset-name (tc/dataset FemPregTable))))

;; A little shorthand...
(def ds FemPregDS)


(comment
  ;; The above can also be accomplished with a vector, but would require something like...

  (tablecloth.api/dataset (rest FemPregtable) {:column-names (first FemPregTable)})

  ;; But...dealing with a map is easier
  )

(comment
  (tc/row-count ds)
  (tc/info ds)
  (tc/info ds :basic)
  (tc/info ds :columns)
  (tc/shape ds)
  (tc/dataset-name ds)
  (ds :stopduse)
  )

;; Clean Variables (This may go in a different namespace)
;; NOTE: this function is overly specific.
(defn clean-fem-preg
  "Cleans variables
  ds: dataset/dataframe"
  [ds]
  (-> ds
      (tc/map-columns :agepreg (fn [x] (/ x 100)))
      )
  )

(comment
  (clean-fem-preg ds)

  (def ds (tc/dataset {:v1 [1 2 2 1 1 2]
                       :v2 [1 4 2 5 3 6]
                       :v3 [0.5 0.5 1.0 1.0 1.5 1.5]
                       :v4 ["A" "A" "B" "B" "C" "C"]}))

  (-> ds
      (tc/map-columns :v2 (fn [x] (/ x 100))))

  )
